#
# Welcome to Jest on BSMCH!
![enter image description here](https://jestjs.io/img/opengraph.png)

Based on:
-  [Jest Tutorial for Beginners: Getting Started With JavaScript Testing](https://www.valentinog.com/blog/jest/)
-  [Modules, introduction](https://javascript.info/modules-intro)

# Setup 
**Only for the first time on a computer with proxy:**
   - Check your node version on pc is **above v13** on CMD run: `node -v` , if it less than 13 , install the LTS (latest stable) version from node site [here](https://nodejs.org/en/) 
![node](https://i.ibb.co/1bcNTfc/node.png)
- After installing on CMD run again: node -v and check that everything is good: 
![everything is good](https://i.ibb.co/0ZnhnCL/ddsds.png)

On CMD run this two commands to config the proxy:
 - `npm config set proxy http://10.0.0.10:80`
 - `npm config set https-proxy http://10.0.0.10:80`
---
Open CMD on project directory:

 1. Add npm to the project: `npm init -y`
 2. Install jest on the project: `npm i jest --save-dev`
 3. Configure jest as the test run engine in the project: 
 edit package.json file (located in the project folder)

 **from :** 
 ```json
 "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
```

 **to :** 
 ```json
 "scripts": {
    "test": "node --experimental-vm-modules node_modules/jest/bin/jest.js"
  },
  "type": "module"
```

(What we did was to set up a script called test, which runs JEST)

4. Create folder named: \_\_tests\_\_ , you can use CMD command : `mkdir __tests__` . This folder will contain all the tests you will write.

If you did everything right, your project should look like this:
![project](https://i.ibb.co/Ws3dYmm/project.png)

# Create Test files
As mentioned earlier, \_\_tests\_\_ needs to conatins the tests, **therefore, every test file we create must be in this folder.**

**Create a test file:**
We will create a test file for each JS file on which we want to run tests.

 If the js file named: `main.js` , so the test file will be called `main.spec.js` .

Lets say that main.js contains the function:
```js
// main.js

const exampleFunction = () => {
	console.log("R-L");
}

const bestFunction = () => {
	console.log("Ush");
}
```
How can we test those functions on main.spec.js? Its a diffrent js file..
The answer is Modules!

### Modules

A module is just a file. One script is one module. As simple as that.

Modules can load each other and use special directives  `export`  and  `import`  to interchange functionality, call functions of one module from another one:

-   `export`  keyword labels variables and functions that should be accessible from outside the current module.
-   `import`  allows the import of functionality from other modules.

**Setup**
Once we want to import something out of a js file, we have to declare on the html who uses this js file, that the js file is a module type.

```html
<script type="module" src="main.js"></script>
```
now lets export the function we want by adding `export` to the function:
```js
// main.js 

export const exampleFunction = () => {
	console.log("R-L");
}

export const bestFunction = () => {
	console.log("Ush");
}
```

and the import on the test file:
```js
// main.spec.js 

import {exampleFunction, bestFunction} from '../main';
```

# Writing Tests:
